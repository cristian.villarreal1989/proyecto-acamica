const chalk = require ('chalk');

function sumar (n1,n2){

    let result = n1 + n2;
    console.log(chalk.blue(`${n1} + ${n2} = ${result}` ));
    
}

function restar (n1, n2){

    console.log(n1 - n2);
    
}

function multiplicar (n1, n2){

    console.log(n1 * n2);
}

function dividir (n1, n2){

    console.log(n1 / n2);
}

exports.sumar = sumar;
exports.restar = restar;
exports.multiplicar = multiplicar;
exports.dividir = dividir;
