let autores = require('./autores');

const buscarAutor = (id) => {


    const resultado = autores.autores.filter(autor => autor.idAutor == id);
    return resultado;


}

const autorExiste = (id) => {

    const existe = autores.autores.filter(autor => autor.idAutor == id);

    if (existe.length > 0) {
        return true;
    } else {
        return false;
    }

}

const midelAutor = (req, res, next) => {
    const id = req.params.id;
    const autorE = autores.autores.filter(autor => autor.idAutor == id);

    if (autorE.length > 0) {
        
        next();
    } else {
        res.json({ msj: "El autor no existe, entre a /autores" });
        next();
    }

}

const todosAutores = () => {

    return autores;
}

const agregarAutor = (autorNuevo) => {

    autores.autores.push(autorNuevo);
    return "Autor agregado correctamente";

}

const deleteAutor = (id) => {

        autores.autores = autores.autores.filter(autor => autor.idAutor != id);
 

}

const buscarIndice = (id) => {
   
    return autores.autores.findIndex(x => x.idAutor == id);

}

const modificarAutor = (id, object) => {
    object = {
        idAutor: id,
        nombre: object.nombre,
        apellido: object.apellido,
        fechaDeNacimiento: object.fechaDeNacimiento,
        libros: []
    }

    autores.autores[buscarIndice(id)] = object;



    return "El autor fue modificado";

}

//FUNCIONES DE LIBROS 

const mostarLibros = (id) => {

    return buscarAutor(id)[0].libros;

}

const agregarLibro = (id,libroNuevo) => {
    const indice = buscarIndice(id);
    autores.autores[indice].libros.push(libroNuevo);
    return "Libro agregado correctamente";
}

const buscarLibro = (id,idLibro) => {
    const indiceAutor = buscarIndice(id);
    
    return autores.autores[indiceAutor].libros.filter(libro => libro.id == idLibro);
    
}

const deleteLibro = (id, idLibro) => {

    const indiceAutor = buscarIndice(id);
    
    autores.autores[indiceAutor].libros = autores.autores[indiceAutor].libros.filter(libro => libro.id != idLibro);

    return "Libro Borrado Correctamente";
}

const midelLibro = (req,res,next) => {
    
    const idAutor = req.params.id;
    const idLibro = req.params.idLibros;
    const libroEncontrado = buscarLibro(idAutor,idLibro);

    if (libroEncontrado.length > 0) {
        
        next();
    } else {
        res.json({ msj: "El libro no existe" });
        next();
    }
    
}


exports.buscarAutor = buscarAutor;
exports.todosAutores = todosAutores;
exports.autorExiste = autorExiste;
exports.agregarAutor = agregarAutor;
exports.deleteAutor = deleteAutor;
exports.modificarAutor = modificarAutor;
exports.midelAutor = midelAutor;
exports.mostarLibros = mostarLibros;
exports.agregarLibro = agregarLibro;
exports.buscarLibro = buscarLibro;
exports.deleteLibro = deleteLibro;
exports.midelLibro = midelLibro;
