const express = require ('express');
const app = express();
const port = 3007;
let autores = require ('./autores');
const funciones = require('./funciones');




//para el post Utilizar estas dos funciones

app.use(express.urlencoded({extended: true}));
app.use(express.json());


app.get('/autores', function(req,res){

 res.json(funciones.todosAutores());

} )

app.get('/autores/:id', funciones.midelAutor, function (req,res){
const autorEncontrado = funciones.buscarAutor(req.params.id);

res.json({msj:autorEncontrado});

})

app.delete('/autores/:id',function (req,res){
  
  res.json(funciones.deleteAutor(req.params.id))
}
)

app.put('/autores/:id', funciones.midelAutor, function (req,res){
  
  const modificarAutor = funciones.modificarAutor(req.params.id, req.body);
  res.json(modificarAutor);
})



app.post('/autores',function (req,res){
  
  const autorAgregado = funciones.agregarAutor(req.body);
  res.json({msj:autorAgregado});

}
)

//RUTAS DE LIBROS 

app.get('/autores/:id/libros', funciones.midelAutor, function (req,res){

  res.json(funciones.mostarLibros(req.params.id));

})

app.post('/autores/:id/libros', funciones.midelAutor, function (req,res){
  const libroNuevo = funciones.agregarLibro(req.params.id, req.body);
  res.json({msj:libroNuevo});

}
)

app.get('/autores/:id/libros/:idLibros', funciones.midelAutor, funciones.midelLibro, function (req,res){

  const libro = funciones.buscarLibro(req.params.id,req.params.idLibros);
  res.json({libro});

}
)

app.delete('/autores/:id/libros/:idLibros', funciones.midelAutor, funciones.midelLibro, function (req,res){

   const borrarLibro = funciones.deleteLibro(req.params.id, req.params.idLibros);

   res.json(borrarLibro);


})


app.listen(port, function (){
console.log(`Corriendo en el puerto http://localhost:${port}`);

})



