autores = [
    {
      idAutor: 1,
      nombre: "Reese Santos",
      apellido: "Frank",
      fechaDeNacimiento: "18/04/2015",
      libros: [
        {
          id: 1,
          titulo: "Cantu Russell",
          descripcion: "Reprehenderit tempor officia consequat eu laboris labore nisi laboris. Eiusmod excepteur incididunt magna non officia sint labore sint reprehenderit magna velit anim nostrud ut. Anim exercitation sit qui anim ex aute ipsum. In pariatur ex enim sint est cupidatat magna culpa ad labore.\r\n",
          anioPublicacion: 2020
        },
        {
          id: 2,
          titulo: "Savage Houston",
          descripcion: "Ipsum duis fugiat elit duis ullamco ad laborum irure consectetur nostrud. Veniam elit culpa qui commodo eiusmod excepteur nostrud consectetur in fugiat. Aute in et aute ullamco ipsum ut nisi cupidatat tempor.\r\n",
          anioPublicacion: 2018
        },
        {
          id: 3,
          titulo: "Beach Stein",
          descripcion: "Aliquip tempor labore pariatur et minim minim cupidatat laboris esse pariatur officia. Veniam occaecat esse eu do dolore sunt ad cupidatat magna tempor. Elit laboris irure nulla consequat eiusmod occaecat labore nostrud anim. Nostrud pariatur elit qui sint laborum magna officia. In id ad pariatur enim nisi et laborum excepteur et officia mollit do cupidatat laborum. Laborum occaecat consectetur ex incididunt esse qui reprehenderit consectetur et culpa sunt proident ipsum minim. Adipisicing laborum aute consequat exercitation.\r\n",
          anioPublicacion: 2017
        }
      ]
    },
    {
      idAutor: 2,
      nombre: "Buckley Abbott",
      apellido: "Newton",
      fechaDeNacimiento: "17/07/2018",
      libros: [
        {
          id: 1,
          titulo: "Melisa Norton",
          descripcion: "Incididunt et in irure ipsum cillum tempor sunt. Ut proident tempor velit duis non et tempor ex. Ipsum non sunt veniam excepteur consequat veniam enim ut sunt et eu irure Lorem. Magna dolore sit non dolore laboris sit laboris cupidatat anim proident do eiusmod irure. Aliquip anim ut irure Lorem officia.\r\n",
          anioPublicacion: 2018
        },
        {
          id: 2,
          titulo: "James Clements",
          descripcion: "Aliquip elit eu commodo et eiusmod occaecat labore deserunt. Veniam esse ut adipisicing minim aute amet culpa laboris eiusmod tempor nulla elit. Aliquip non eu elit sit excepteur. Eiusmod tempor excepteur ullamco eiusmod veniam veniam incididunt incididunt est mollit.\r\n",
          anioPublicacion: 2020
        },
        {
          id: 3,
          titulo: "Rosie Santana",
          descripcion: "Amet incididunt cupidatat adipisicing Lorem eu eiusmod quis. Qui ex cillum eu duis laboris non fugiat ullamco est quis labore reprehenderit. Non non consectetur adipisicing nostrud nostrud sit mollit ad do voluptate in.\r\n",
          anioPublicacion: 2016
        }
      ]
    },
    {
      idAutor: 3,
      nombre: "Jacquelyn Crane",
      apellido: "Wall",
      fechaDeNacimiento: "21/11/2019",
      libros: [
        {
          id: 1,
          titulo: "Heidi Kane",
          descripcion: "Sunt Lorem anim sit eiusmod mollit ipsum dolor culpa mollit sint. Pariatur esse voluptate quis laborum minim ex elit irure. Aliqua ut proident exercitation nostrud aliqua dolor. Ex sint velit cillum laboris deserunt deserunt dolore ut. Anim nulla voluptate veniam exercitation cillum veniam amet ea deserunt eu duis nulla.\r\n",
          anioPublicacion: 2018
        },
        {
          id: 2,
          titulo: "John Wyatt",
          descripcion: "Dolor laboris minim amet do ullamco ut. Pariatur voluptate nostrud elit nisi voluptate laboris duis do. Eiusmod sint veniam duis non excepteur consectetur ut.\r\n",
          anioPublicacion: 2017
        },
        {
          id: 3,
          titulo: "Kitty Forbes",
          descripcion: "Eiusmod dolore Lorem nisi magna do incididunt ullamco aliqua laboris aliquip eiusmod est. Reprehenderit eu amet ex veniam tempor consequat nulla laborum occaecat et laborum. Irure cupidatat quis dolor anim aliqua esse dolore anim officia. Lorem eiusmod laborum magna incididunt labore aliqua aliquip commodo incididunt qui laborum. Officia veniam aliqua occaecat qui pariatur aliquip culpa ullamco culpa officia elit nostrud et.\r\n",
          anioPublicacion: 2018
        }
      ]
    },
    {
      idAutor: 4,
      nombre: "Foreman Shields",
      apellido: "Mcclain",
      fechaDeNacimiento: "28/06/2019",
      libros: [
        {
          id: 1,
          titulo: "Tracy Payne",
          descripcion: "Commodo id sit et duis consequat sunt tempor voluptate nisi. Fugiat amet dolore non duis quis tempor nulla reprehenderit quis ut cupidatat eiusmod proident eu. Consequat duis ipsum qui sint ea ad incididunt. Aute et elit ut amet incididunt. Do esse duis eu deserunt est irure non deserunt commodo dolore Lorem laborum veniam.\r\n",
          anioPublicacion: 2016
        },
        {
          id: 2,
          titulo: "House Wise",
          descripcion: "Pariatur qui tempor cupidatat dolor aliqua. Laboris minim ut incididunt est laboris ut cillum irure sit et anim elit non. Exercitation est labore cupidatat veniam excepteur ipsum consequat velit nulla dolor laborum deserunt. Qui nostrud ut nisi laboris id ex culpa non ipsum veniam sunt pariatur. Sint mollit irure sit pariatur ullamco laboris. Dolore consectetur ut ut duis ullamco commodo anim id culpa minim et qui laborum cillum.\r\n",
          anioPublicacion: 2019
        },
        {
          id: 3,
          titulo: "Shannon Hood",
          descripcion: "Veniam Lorem sint voluptate nostrud. Eiusmod dolore enim enim laborum amet id nisi culpa. Anim esse dolor est aliqua dolore amet occaecat in fugiat sit incididunt aliquip veniam mollit. Dolor do est esse in mollit dolore labore deserunt ex proident.\r\n",
          anioPublicacion: 2019
        }
      ]
    },
    {
      idAutor: 5,
      nombre: "Whitney Pugh",
      apellido: "Skinner",
      fechaDeNacimiento: "12/06/2016",
      libros: [
        {
          id: 1,
          titulo: "Griffin Burnett",
          descripcion: "Tempor ea ad eiusmod nostrud consequat fugiat ad enim. Voluptate consectetur velit sunt adipisicing cillum culpa dolor non adipisicing esse dolor ipsum. Deserunt sunt non anim adipisicing anim reprehenderit quis. Consequat aliquip enim amet et laborum nisi magna ipsum esse culpa.\r\n",
          anioPublicacion: 2017
        },
        {
          id: 2,
          titulo: "Cunningham Pierce",
          descripcion: "Culpa fugiat tempor veniam aliquip in proident. Elit sit eiusmod ut nisi do reprehenderit ut ea culpa est in reprehenderit nisi. Lorem voluptate elit non ad amet commodo cupidatat magna. Aliquip in non duis eu mollit aliqua cillum proident cillum ipsum sit.\r\n",
          anioPublicacion: 2015
        },
        {
          id: 3,
          titulo: "Erica Rodriguez",
          descripcion: "Eiusmod eiusmod ut enim qui elit. Ut sunt id eu pariatur anim cillum Lorem dolore sint eiusmod. Ex quis nulla incididunt ipsum non elit tempor commodo excepteur est cillum fugiat. Lorem magna esse anim laborum Lorem ullamco fugiat laborum cillum esse irure sit.\r\n",
          anioPublicacion: 2019
        }
      ]
    }
];

exports.autores = autores;